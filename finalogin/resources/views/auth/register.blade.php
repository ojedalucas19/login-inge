<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
</head>
<body>

    <div>
        <h2>Registro</h2>

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <label for="name">Nombre:</label>
            <input type="text" name="name" value="{{ old('name') }}" required>

            <label for="email">Email:</label>
            <input type="email" name="email" value="{{ old('email') }}" required>

            <label for="password">Contraseña:</label>
            <input type="password" name="password" required>

            <label for="password_confirmation">Confirmar Contraseña:</label>
            <input type="password" name="password_confirmation" required>

            <button type="submit">Registrarse</button>
        </form>

        <p>¿Ya tienes una cuenta? <a href="{{ route('login') }}">Inicia sesión</a></p>
    </div>

</body>
</html>
