<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>
<body>

    <div>
        <h2>Bienvenido</h2>
        <p>¡Has iniciado sesión con éxito!</p>

        {{-- Contenido adicional de la página de inicio --}}

        <form method="POST" action="{{ url('/logout') }}">
            @csrf
            <button type="submit">Cerrar sesión</button>
        </form>
    </div>

</body>
</html>
